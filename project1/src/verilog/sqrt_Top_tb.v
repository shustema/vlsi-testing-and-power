module sqrt_Top_tb();

reg [6:0] num;
wire [3:0] result;
reg start, clk, clear, reset;
wire ready;

sqrt_Top uut(.start(start), .clk(clk), .clear(clear), .num(num), .result(result), .ready(ready), .reset(reset));

initial begin 
reset = 0;
reset = 1;

// begin at state 00
clk = 0;
clear = 1;
#10;
clk = 1;
#10;
clear = 0;
clk = 0;
start = 0;
#10; 
clk = 0;
clear = 0;
num = 0010000; //number to be loaded into register Rdata is 16 (decimal) expecting 4 in output
start = 1;
#10;
clk = 1;
#10; // loading register state 01
clk = 0;
start = 0; //reset start input to zero, since it is not needed until we give another number to sqrt
#10; // next state is 02

clk = 1;
#10; // in state 02
clk = 0;
#10; // next state is 03

clk = 1;
#10; // in state 03
clk = 0;
#10; // next state will be 02 and with it will increase delta by 2

clk = 1;
#10; // in state 02, Rdelta has increased from 3 to 5, Rn is increased from 1 to 4, next state will be 03
clk = 0;
#10;

clk = 1;
#10; // in state 03, next state will be 02 and it will increase the delta by 2 again.
clk = 0;
#10;

clk = 1;
#10; // in state 02, Rdelta increased from 5 to 7, Rn is increased from 4 to 9, next state will be 03
clk = 0;
#10; 

clk = 1;
#10; // in state 03, next state will be 02 and it will increase the delta by 2 again.
clk = 0;
#10;

clk = 1;
#10; // in state 02, Rdelta increased from 7 to 9, Rn is increased from 9 to 16, next state will be 03
clk = 0;
#10;

// the controller will now receive the "finished" signal and will stop the process

clk = 1;
#10; // in state 00, wait for start signal.
clk = 0;
#10;

// a few clock ticks will happen to make sure the count on Rdelta will not increase further.
clk = 1; #10;
num = 0000000;
clk = 0; clear = 1; #10;
clk = 1; #10;
clk = 0; #10;
clk = 1; #10; 
clk = 0; clear = 0; #10;
clk = 1; #10;
clk = 0; #10;
clk = 1; #10;
clk = 0; #10;

clk = 0;
clear = 1;
#10;
clk = 1;
#10;
clear = 0;
clk = 0;
start = 0;
#10; 
clk = 0;
clear = 0;
num = 0000001; //number to be loaded into register Rdata is 1 (decimal) expecting 1 in output
start = 1;
#10;
clk = 1;
#10; // loading register state 01
clk = 0;
start = 0; 
#10; 

clk = 1;
#10; 
clk = 0;
#10; 

clk = 1;
#10; 
clk = 0;
#10; 

clk = 1;
#10; 
clk = 0;
#10;

clk = 1;
#10; 
clk = 0;
#10;

clk = 1;
#10; 
clk = 0;
#10; 

clk = 1;
#10; 
clk = 0;
#10;

clk = 1;
#10; 
clk = 0;
#10;

clk = 1;  #10; 
clk = 0;  #10;

clk = 1; #10; 
clk = 0; #10;
clk = 1; #10;
clk = 0; #10;
clk = 1; #10;
clk = 0; #10;

end
endmodule
