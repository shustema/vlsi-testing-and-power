/////////////////////////////////////////////////////////////
// Created by: Synopsys Design Compiler(R)
// Version   : O-2018.06-SP1
// Date      : Thu Feb 21 18:47:20 2019
/////////////////////////////////////////////////////////////


module sqrt_controller ( start, finish, clk, clear, ready, load_data, 
        incr_delta, find_next_sq );
  input start, finish, clk, clear;
  output ready, load_data, incr_delta, find_next_sq;
  wire   n2, n4, n6, n7, n8, n3, n5;
  wire   [1:0] state;
  wire   [1:0] next_state;

  DFFARX1 \state_reg[0]  ( .D(next_state[0]), .CLK(clk), .RSTB(n5), .Q(
        state[0]), .QN(n4) );
  DFFARX1 \state_reg[1]  ( .D(next_state[1]), .CLK(clk), .RSTB(n5), .Q(
        state[1]), .QN(n2) );
  AO21X1 U9 ( .IN1(state[1]), .IN2(n3), .IN3(state[0]), .Q(next_state[1]) );
  NAND3X0 U10 ( .IN1(n4), .IN2(n2), .IN3(start), .QN(n7) );
  AND2X1 U11 ( .IN1(start), .IN2(ready), .Q(load_data) );
  XOR2X1 U12 ( .IN1(state[1]), .IN2(state[0]), .Q(n8) );
  NAND3X0 U13 ( .IN1(n3), .IN2(n4), .IN3(state[1]), .QN(n6) );
  INVX0 U3 ( .INP(n6), .ZN(find_next_sq) );
  NOR2X0 U4 ( .IN1(n4), .IN2(n8), .QN(incr_delta) );
  NOR2X0 U5 ( .IN1(n8), .IN2(state[0]), .QN(ready) );
  NAND2X1 U6 ( .IN1(n6), .IN2(n7), .QN(next_state[0]) );
  INVX0 U7 ( .INP(finish), .ZN(n3) );
  INVX0 U8 ( .INP(clear), .ZN(n5) );
endmodule


module sqrt_data_path_DW01_add_0 ( A, B, CI, SUM, CO );
  input [6:0] A;
  input [6:0] B;
  output [6:0] SUM;
  input CI;
  output CO;
  wire   n1, n2;
  wire   [6:1] carry;

  FADDX1 U1_4 ( .A(A[4]), .B(B[4]), .CI(carry[4]), .CO(carry[5]), .S(SUM[4])
         );
  FADDX1 U1_3 ( .A(A[3]), .B(B[3]), .CI(carry[3]), .CO(carry[4]), .S(SUM[3])
         );
  FADDX1 U1_2 ( .A(A[2]), .B(B[2]), .CI(carry[2]), .CO(carry[3]), .S(SUM[2])
         );
  FADDX1 U1_1 ( .A(A[1]), .B(B[1]), .CI(n1), .CO(carry[2]), .S(SUM[1]) );
  AND2X1 U1 ( .IN1(B[0]), .IN2(A[0]), .Q(n1) );
  XNOR2X1 U2 ( .IN1(n2), .IN2(A[6]), .Q(SUM[6]) );
  NAND2X1 U3 ( .IN1(carry[5]), .IN2(A[5]), .QN(n2) );
  XOR2X1 U4 ( .IN1(carry[5]), .IN2(A[5]), .Q(SUM[5]) );
  XOR2X1 U5 ( .IN1(B[0]), .IN2(A[0]), .Q(SUM[0]) );
endmodule


module sqrt_data_path ( num, load_data, incr_delta, find_next_sq, clk, finish, 
        result, reset );
  input [6:0] num;
  output [3:0] result;
  input load_data, incr_delta, find_next_sq, clk, reset;
  output finish;
  wire   N8, N9, N10, N11, N13, N14, N15, N16, N17, N18, N19, N20, N21, N22,
         N23, N24, N25, N26, N27, N28, N29, N30, N31, N32, N34, N35, n10, n12,
         n19, n21, n22, n23, n26, n27, n54, n55, n56, n57, n58, n59, n60, n61,
         n62, n63, n64, n65, n66, n67, n68, n69, n70, n71, n72, n73,
         \add_19_S2/carry[4] , \add_19_S2/carry[3] , n1, n2, n4, n5, n6, n7,
         n8, n9, n11, n13, n14, n15, n16, n17, n18, n20, n24, n25, n28, n29,
         n30, n31, n32, n33, n34, n35, n36, n37;
  wire   [6:0] Rn;

  DFFX1 \Rdata_reg[6]  ( .D(n73), .CLK(clk), .Q(n9) );
  DFFX1 \Rdata_reg[5]  ( .D(n72), .CLK(clk), .Q(n11) );
  DFFX1 \Rdata_reg[4]  ( .D(n71), .CLK(clk), .Q(n5) );
  DFFX1 \Rdata_reg[3]  ( .D(n70), .CLK(clk), .Q(n13) );
  DFFX1 \Rdata_reg[2]  ( .D(n69), .CLK(clk), .Q(n6) );
  DFFX1 \Rdata_reg[1]  ( .D(n68), .CLK(clk), .Q(n7) );
  DFFX1 \Rdata_reg[0]  ( .D(n67), .CLK(clk), .Q(n8) );
  DFFX1 \Rdelta_reg[0]  ( .D(n66), .CLK(clk), .Q(N11) );
  DFFX1 \Rdelta_reg[1]  ( .D(n65), .CLK(clk), .Q(result[0]), .QN(n4) );
  DFFX1 \Rdelta_reg[2]  ( .D(n64), .CLK(clk), .Q(result[1]), .QN(n1) );
  DFFX1 \Rdelta_reg[3]  ( .D(n63), .CLK(clk), .Q(result[2]) );
  DFFX1 \Rdelta_reg[4]  ( .D(n62), .CLK(clk), .Q(result[3]) );
  DFFX1 \Rn_reg[0]  ( .D(n61), .CLK(clk), .Q(Rn[0]) );
  DFFX1 \Rn_reg[1]  ( .D(n60), .CLK(clk), .Q(Rn[1]) );
  DFFX1 \Rn_reg[2]  ( .D(n59), .CLK(clk), .Q(Rn[2]) );
  DFFX1 \Rn_reg[3]  ( .D(n58), .CLK(clk), .Q(Rn[3]) );
  DFFX1 \Rn_reg[4]  ( .D(n57), .CLK(clk), .Q(Rn[4]) );
  DFFX1 \Rn_reg[5]  ( .D(n56), .CLK(clk), .Q(Rn[5]) );
  DFFX1 \Rn_reg[6]  ( .D(n55), .CLK(clk), .Q(Rn[6]) );
  DFFX1 finish_reg ( .D(n54), .CLK(clk), .Q(finish) );
  AO22X1 U3 ( .IN1(reset), .IN2(finish), .IN3(N35), .IN4(n37), .Q(n54) );
  AO22X1 U5 ( .IN1(n10), .IN2(Rn[6]), .IN3(N27), .IN4(n12), .Q(n55) );
  AO22X1 U10 ( .IN1(n10), .IN2(Rn[5]), .IN3(N26), .IN4(n12), .Q(n56) );
  AO22X1 U11 ( .IN1(n10), .IN2(Rn[4]), .IN3(N25), .IN4(n12), .Q(n57) );
  AO22X1 U12 ( .IN1(n10), .IN2(Rn[3]), .IN3(N24), .IN4(n12), .Q(n58) );
  AO22X1 U13 ( .IN1(n10), .IN2(Rn[2]), .IN3(N23), .IN4(n12), .Q(n59) );
  AO22X1 U14 ( .IN1(n10), .IN2(Rn[1]), .IN3(N22), .IN4(n12), .Q(n60) );
  OR2X1 U18 ( .IN1(reset), .IN2(N21), .Q(n19) );
  AO22X1 U20 ( .IN1(result[3]), .IN2(n21), .IN3(N15), .IN4(n22), .Q(n62) );
  AO22X1 U21 ( .IN1(result[2]), .IN2(n21), .IN3(N14), .IN4(n22), .Q(n63) );
  AO22X1 U22 ( .IN1(result[1]), .IN2(n21), .IN3(N13), .IN4(n22), .Q(n64) );
  OR2X1 U25 ( .IN1(reset), .IN2(n4), .Q(n23) );
  OR2X1 U28 ( .IN1(reset), .IN2(N11), .Q(n26) );
  AO22X1 U30 ( .IN1(num[0]), .IN2(n27), .IN3(n35), .IN4(n8), .Q(n67) );
  AO22X1 U31 ( .IN1(num[1]), .IN2(n27), .IN3(n35), .IN4(n7), .Q(n68) );
  AO22X1 U32 ( .IN1(num[2]), .IN2(n27), .IN3(n35), .IN4(n6), .Q(n69) );
  AO22X1 U33 ( .IN1(num[3]), .IN2(n27), .IN3(n35), .IN4(n13), .Q(n70) );
  AO22X1 U34 ( .IN1(num[4]), .IN2(n27), .IN3(n35), .IN4(n5), .Q(n71) );
  AO22X1 U35 ( .IN1(num[5]), .IN2(n27), .IN3(n35), .IN4(n11), .Q(n72) );
  AO22X1 U36 ( .IN1(num[6]), .IN2(n27), .IN3(n35), .IN4(n9), .Q(n73) );
  AO22X1 U39 ( .IN1(num[1]), .IN2(load_data), .IN3(n36), .IN4(n7), .Q(N9) );
  AO22X1 U41 ( .IN1(num[2]), .IN2(load_data), .IN3(n36), .IN4(n6), .Q(N8) );
  AO22X1 U51 ( .IN1(n33), .IN2(Rn[0]), .IN3(find_next_sq), .IN4(N21), .Q(N34)
         );
  AO22X1 U55 ( .IN1(n33), .IN2(Rn[2]), .IN3(find_next_sq), .IN4(N23), .Q(N32)
         );
  AO22X1 U57 ( .IN1(n33), .IN2(Rn[3]), .IN3(find_next_sq), .IN4(N24), .Q(N31)
         );
  AO22X1 U59 ( .IN1(n33), .IN2(Rn[4]), .IN3(find_next_sq), .IN4(N25), .Q(N30)
         );
  AO22X1 U61 ( .IN1(n33), .IN2(Rn[5]), .IN3(find_next_sq), .IN4(N26), .Q(N29)
         );
  AO22X1 U63 ( .IN1(n33), .IN2(Rn[6]), .IN3(find_next_sq), .IN4(N27), .Q(N28)
         );
  AO22X1 U66 ( .IN1(n34), .IN2(N11), .IN3(incr_delta), .IN4(N11), .Q(N20) );
  AO22X1 U68 ( .IN1(result[0]), .IN2(n34), .IN3(n4), .IN4(incr_delta), .Q(N19)
         );
  AO22X1 U69 ( .IN1(result[1]), .IN2(n34), .IN3(incr_delta), .IN4(N13), .Q(N18) );
  AO22X1 U70 ( .IN1(result[2]), .IN2(n34), .IN3(incr_delta), .IN4(N14), .Q(N17) );
  AO22X1 U71 ( .IN1(result[3]), .IN2(n34), .IN3(incr_delta), .IN4(N15), .Q(N16) );
  AO22X1 U73 ( .IN1(num[0]), .IN2(load_data), .IN3(n36), .IN4(n8), .Q(N10) );
  sqrt_data_path_DW01_add_0 add_20_S2 ( .A(Rn), .B({1'b0, 1'b0, N16, N17, N18, 
        N19, N20}), .CI(1'b0), .SUM({N27, N26, N25, N24, N23, N22, N21}) );
  MUX21X1 U4 ( .IN1(n23), .IN2(result[0]), .S(n21), .Q(n65) );
  NOR2X0 U6 ( .IN1(n1), .IN2(n4), .QN(\add_19_S2/carry[3] ) );
  MUX21X1 U7 ( .IN1(n19), .IN2(Rn[0]), .S(n10), .Q(n61) );
  MUX21X1 U9 ( .IN1(n26), .IN2(N11), .S(n21), .Q(n66) );
  AOI22X1 U15 ( .IN1(num[4]), .IN2(load_data), .IN3(n36), .IN4(n5), .QN(n2) );
  INVX0 U16 ( .INP(N32), .ZN(n32) );
  INVX0 U17 ( .INP(find_next_sq), .ZN(n33) );
  INVX0 U19 ( .INP(load_data), .ZN(n36) );
  INVX0 U23 ( .INP(n27), .ZN(n35) );
  INVX0 U24 ( .INP(incr_delta), .ZN(n34) );
  AOI22X1 U26 ( .IN1(n33), .IN2(Rn[1]), .IN3(find_next_sq), .IN4(N22), .QN(n14) );
  INVX0 U27 ( .INP(reset), .ZN(n37) );
  INVX0 U29 ( .INP(N8), .ZN(n31) );
  NOR2X0 U37 ( .IN1(n36), .IN2(reset), .QN(n27) );
  AOI22X1 U38 ( .IN1(num[3]), .IN2(load_data), .IN3(n36), .IN4(n13), .QN(n15)
         );
  AOI22X1 U40 ( .IN1(num[5]), .IN2(load_data), .IN3(n36), .IN4(n11), .QN(n16)
         );
  AOI22X1 U42 ( .IN1(num[6]), .IN2(load_data), .IN3(n36), .IN4(n9), .QN(n17)
         );
  NOR2X0 U43 ( .IN1(find_next_sq), .IN2(reset), .QN(n10) );
  NOR2X0 U44 ( .IN1(incr_delta), .IN2(reset), .QN(n21) );
  NOR2X0 U45 ( .IN1(n10), .IN2(reset), .QN(n12) );
  NOR2X0 U46 ( .IN1(n21), .IN2(reset), .QN(n22) );
  XOR2X1 U47 ( .IN1(\add_19_S2/carry[4] ), .IN2(result[3]), .Q(N15) );
  AND2X1 U48 ( .IN1(result[2]), .IN2(\add_19_S2/carry[3] ), .Q(
        \add_19_S2/carry[4] ) );
  XOR2X1 U49 ( .IN1(\add_19_S2/carry[3] ), .IN2(result[2]), .Q(N14) );
  XOR2X1 U50 ( .IN1(result[0]), .IN2(result[1]), .Q(N13) );
  NOR2X0 U52 ( .IN1(N9), .IN2(n14), .QN(n18) );
  NOR2X0 U53 ( .IN1(N34), .IN2(n18), .QN(n20) );
  AOI222X1 U54 ( .IN1(N8), .IN2(n32), .IN3(n20), .IN4(N10), .IN5(N9), .IN6(n14), .QN(n24) );
  AO221X1 U56 ( .IN1(N32), .IN2(n31), .IN3(N31), .IN4(n15), .IN5(n24), .Q(n25)
         );
  OA221X1 U58 ( .IN1(N30), .IN2(n2), .IN3(N31), .IN4(n15), .IN5(n25), .Q(n28)
         );
  AO221X1 U60 ( .IN1(N30), .IN2(n2), .IN3(N29), .IN4(n16), .IN5(n28), .Q(n29)
         );
  OA221X1 U62 ( .IN1(N28), .IN2(n17), .IN3(N29), .IN4(n16), .IN5(n29), .Q(n30)
         );
  AO21X1 U64 ( .IN1(N28), .IN2(n17), .IN3(n30), .Q(N35) );
endmodule


module sqrt_Top ( start, clk, clear, num, result, ready, reset );
  input [6:0] num;
  output [3:0] result;
  input start, clk, clear, reset;
  output ready;
  wire   finish, load_data, incr_delta, find_next_sq;

  sqrt_controller m1 ( .start(start), .finish(finish), .clk(clk), .clear(clear), .ready(ready), .load_data(load_data), .incr_delta(incr_delta), 
        .find_next_sq(find_next_sq) );
  sqrt_data_path m2 ( .num(num), .load_data(load_data), .incr_delta(incr_delta), .find_next_sq(find_next_sq), .clk(clk), .finish(finish), .result(result), 
        .reset(reset) );
endmodule

