/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Expert(TM) in wire load mode
// Version   : O-2018.06-SP1
// Date      : Wed Feb 20 12:26:01 2019
/////////////////////////////////////////////////////////////


module sqrt_data_path_DW01_add_0 ( A, B, CI, SUM, CO );
  input [6:0] A;
  input [6:0] B;
  output [6:0] SUM;
  input CI;
  output CO;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11;

  XOR2X1 U1 ( .IN1(A[6]), .IN2(n1), .Q(SUM[6]) );
  NOR2X0 U2 ( .IN1(n2), .IN2(n3), .QN(n1) );
  INVX0 U3 ( .INP(A[5]), .ZN(n3) );
  XNOR2X1 U4 ( .IN1(A[5]), .IN2(n2), .Q(SUM[5]) );
  OAI22X1 U5 ( .IN1(n4), .IN2(A[4]), .IN3(n5), .IN4(B[4]), .QN(n2) );
  AND2X1 U6 ( .IN1(A[4]), .IN2(n4), .Q(n5) );
  XOR3X1 U7 ( .IN1(B[4]), .IN2(A[4]), .IN3(n4), .Q(SUM[4]) );
  AO22X1 U8 ( .IN1(A[3]), .IN2(n6), .IN3(B[3]), .IN4(n7), .Q(n4) );
  OR2X1 U9 ( .IN1(n6), .IN2(A[3]), .Q(n7) );
  XOR3X1 U10 ( .IN1(B[3]), .IN2(A[3]), .IN3(n6), .Q(SUM[3]) );
  AO22X1 U11 ( .IN1(A[2]), .IN2(n8), .IN3(B[2]), .IN4(n9), .Q(n6) );
  OR2X1 U12 ( .IN1(n8), .IN2(A[2]), .Q(n9) );
  XOR3X1 U13 ( .IN1(B[2]), .IN2(A[2]), .IN3(n8), .Q(SUM[2]) );
  AO22X1 U14 ( .IN1(A[1]), .IN2(n10), .IN3(B[1]), .IN4(n11), .Q(n8) );
  OR2X1 U15 ( .IN1(A[1]), .IN2(n10), .Q(n11) );
  XOR3X1 U16 ( .IN1(B[1]), .IN2(A[1]), .IN3(n10), .Q(SUM[1]) );
  AND2X1 U17 ( .IN1(B[0]), .IN2(A[0]), .Q(n10) );
  XOR2X1 U18 ( .IN1(B[0]), .IN2(A[0]), .Q(SUM[0]) );
endmodule


module sqrt_data_path_test_1 ( num, load_data, incr_delta, find_next_sq, clk, 
        finish, result, reset, test_si, test_se );
  input [6:0] num;
  output [3:0] result;
  input load_data, incr_delta, find_next_sq, clk, reset, test_si, test_se;
  output finish;
  wire   n69, n65, n64, n63, n62, n61, n60, n59, n58, n57, n56, n55, n54, n53,
         n52, n51, N27, N26, N25, N24, N23, N22, N21, N19, n1, n75, n66, n106,
         n114, n115, n118, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14,
         n15, n16, n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28,
         n29, n30, n31, n32, n33, n34, n35, n36, n37, n38, n39, n40, n41, n42,
         n43, n44, n45, n46, n47, n48, n49, n50, n72, n76, n85, n105, n107;
  wire   [6:0] Rn;
  assign result[3] = 1'b0;
  assign result[2] = 1'b0;
  assign result[1] = 1'b0;

  sqrt_data_path_DW01_add_0 add_20_S2 ( .A(Rn), .B({1'b0, 1'b0, 1'b0, 1'b0, 
        n118, N19, n1}), .CI(1'b0), .SUM({N27, N26, N25, N24, N23, N22, N21})
         );
  SDFFX1 Rdelta_reg_0_ ( .D(n69), .SI(n72), .SE(test_se), .CLK(clk), .Q(n1), 
        .QN(n106) );
  SDFFX1 Rdata_reg_0_ ( .D(n65), .SI(test_si), .SE(test_se), .CLK(clk), .Q(n48) );
  SDFFX1 Rdata_reg_1_ ( .D(n64), .SI(n48), .SE(test_se), .CLK(clk), .Q(n107)
         );
  SDFFX1 Rdata_reg_2_ ( .D(n63), .SI(n107), .SE(test_se), .CLK(clk), .Q(n49)
         );
  SDFFX1 Rdata_reg_3_ ( .D(n62), .SI(n49), .SE(test_se), .CLK(clk), .Q(n105)
         );
  SDFFX1 Rdata_reg_4_ ( .D(n61), .SI(n105), .SE(test_se), .CLK(clk), .Q(n85)
         );
  SDFFX1 Rdata_reg_5_ ( .D(n60), .SI(n85), .SE(test_se), .CLK(clk), .Q(n50) );
  SDFFX1 Rdata_reg_6_ ( .D(n59), .SI(n50), .SE(test_se), .CLK(clk), .Q(n72) );
  SDFFX1 Rn_reg_0_ ( .D(n58), .SI(result[0]), .SE(test_se), .CLK(clk), .Q(
        Rn[0]), .QN(n75) );
  SDFFX1 Rn_reg_1_ ( .D(n57), .SI(Rn[0]), .SE(test_se), .CLK(clk), .Q(Rn[1])
         );
  SDFFX1 Rn_reg_2_ ( .D(n56), .SI(Rn[1]), .SE(test_se), .CLK(clk), .Q(Rn[2])
         );
  SDFFX1 Rn_reg_3_ ( .D(n55), .SI(Rn[2]), .SE(test_se), .CLK(clk), .Q(Rn[3])
         );
  SDFFX1 Rn_reg_4_ ( .D(n54), .SI(Rn[3]), .SE(test_se), .CLK(clk), .Q(Rn[4])
         );
  SDFFX1 Rn_reg_5_ ( .D(n53), .SI(Rn[4]), .SE(test_se), .CLK(clk), .Q(Rn[5]), 
        .QN(n115) );
  SDFFX1 Rn_reg_6_ ( .D(n52), .SI(Rn[5]), .SE(test_se), .CLK(clk), .Q(Rn[6]), 
        .QN(n114) );
  SDFFX1 finish_reg ( .D(n51), .SI(Rn[6]), .SE(test_se), .CLK(clk), .Q(finish), 
        .QN(n76) );
  SDFFX1 Rdelta_reg_1_ ( .D(n66), .SI(n1), .SE(test_se), .CLK(clk), .Q(
        result[0]), .QN(n3) );
  NAND2X0 U2 ( .IN1(n106), .IN2(n3), .QN(n69) );
  NAND2X0 U3 ( .IN1(n3), .IN2(n4), .QN(n66) );
  MUX21X1 U8 ( .IN1(n48), .IN2(num[0]), .S(n5), .Q(n65) );
  MUX21X1 U9 ( .IN1(n107), .IN2(num[1]), .S(n5), .Q(n64) );
  MUX21X1 U10 ( .IN1(n49), .IN2(num[2]), .S(n5), .Q(n63) );
  MUX21X1 U11 ( .IN1(n105), .IN2(num[3]), .S(n5), .Q(n62) );
  MUX21X1 U12 ( .IN1(n85), .IN2(num[4]), .S(n5), .Q(n61) );
  MUX21X1 U13 ( .IN1(n50), .IN2(num[5]), .S(n5), .Q(n60) );
  MUX21X1 U14 ( .IN1(n72), .IN2(num[6]), .S(n5), .Q(n59) );
  AND2X1 U15 ( .IN1(load_data), .IN2(n3), .Q(n5) );
  MUX21X1 U16 ( .IN1(n6), .IN2(Rn[0]), .S(n7), .Q(n58) );
  NAND2X0 U17 ( .IN1(n3), .IN2(n8), .QN(n6) );
  AO22X1 U18 ( .IN1(n7), .IN2(Rn[1]), .IN3(N22), .IN4(n9), .Q(n57) );
  AO22X1 U19 ( .IN1(n7), .IN2(Rn[2]), .IN3(N23), .IN4(n9), .Q(n56) );
  AO22X1 U20 ( .IN1(n7), .IN2(Rn[3]), .IN3(N24), .IN4(n9), .Q(n55) );
  AO22X1 U21 ( .IN1(n7), .IN2(Rn[4]), .IN3(N25), .IN4(n9), .Q(n54) );
  AO22X1 U22 ( .IN1(n7), .IN2(Rn[5]), .IN3(N26), .IN4(n9), .Q(n53) );
  AO22X1 U23 ( .IN1(n7), .IN2(Rn[6]), .IN3(N27), .IN4(n9), .Q(n52) );
  NOR2X0 U24 ( .IN1(n7), .IN2(result[0]), .QN(n9) );
  NAND2X0 U25 ( .IN1(n10), .IN2(n11), .QN(n51) );
  NAND3X0 U26 ( .IN1(n12), .IN2(Rn[6]), .IN3(n7), .QN(n11) );
  NOR2X0 U27 ( .IN1(result[0]), .IN2(find_next_sq), .QN(n7) );
  MUX21X1 U28 ( .IN1(n76), .IN2(n13), .S(n3), .Q(n10) );
  OA22X1 U29 ( .IN1(n14), .IN2(n15), .IN3(n16), .IN4(n17), .Q(n13) );
  MUX21X1 U30 ( .IN1(n18), .IN2(n19), .S(n14), .Q(n17) );
  AO22X1 U31 ( .IN1(n114), .IN2(n20), .IN3(n21), .IN4(n115), .Q(n19) );
  AO22X1 U32 ( .IN1(n20), .IN2(n22), .IN3(n21), .IN4(n23), .Q(n18) );
  INVX0 U33 ( .INP(N27), .ZN(n22) );
  OA222X1 U34 ( .IN1(n24), .IN2(n25), .IN3(n26), .IN4(n27), .IN5(n21), .IN6(
        n28), .Q(n16) );
  MUX21X1 U35 ( .IN1(n23), .IN2(n115), .S(n14), .Q(n28) );
  INVX0 U36 ( .INP(N26), .ZN(n23) );
  MUX21X1 U37 ( .IN1(n50), .IN2(num[5]), .S(load_data), .Q(n21) );
  MUX21X1 U38 ( .IN1(n85), .IN2(num[4]), .S(load_data), .Q(n27) );
  NOR2X0 U39 ( .IN1(n29), .IN2(n30), .QN(n26) );
  INVX0 U40 ( .INP(n29), .ZN(n25) );
  MUX21X1 U41 ( .IN1(N25), .IN2(Rn[4]), .S(n14), .Q(n29) );
  INVX0 U42 ( .INP(n30), .ZN(n24) );
  AO22X1 U43 ( .IN1(n31), .IN2(n32), .IN3(n33), .IN4(n34), .Q(n30) );
  OR2X1 U44 ( .IN1(n32), .IN2(n31), .Q(n34) );
  INVX0 U45 ( .INP(n35), .ZN(n33) );
  MUX21X1 U46 ( .IN1(n105), .IN2(num[3]), .S(load_data), .Q(n35) );
  AO22X1 U47 ( .IN1(n36), .IN2(n37), .IN3(n38), .IN4(n39), .Q(n32) );
  OR2X1 U48 ( .IN1(n37), .IN2(n36), .Q(n39) );
  MUX21X1 U49 ( .IN1(N23), .IN2(Rn[2]), .S(n14), .Q(n38) );
  INVX0 U50 ( .INP(n40), .ZN(n37) );
  MUX21X1 U51 ( .IN1(n49), .IN2(num[2]), .S(load_data), .Q(n40) );
  OA22X1 U52 ( .IN1(n41), .IN2(n42), .IN3(n43), .IN4(n44), .Q(n36) );
  AND2X1 U53 ( .IN1(n42), .IN2(n41), .Q(n44) );
  INVX0 U54 ( .INP(n45), .ZN(n43) );
  MUX21X1 U55 ( .IN1(n107), .IN2(num[1]), .S(load_data), .Q(n45) );
  MUX21X1 U56 ( .IN1(N22), .IN2(Rn[1]), .S(n14), .Q(n42) );
  NAND2X0 U57 ( .IN1(n46), .IN2(n47), .QN(n41) );
  MUX21X1 U58 ( .IN1(n8), .IN2(n75), .S(n14), .Q(n47) );
  INVX0 U59 ( .INP(N21), .ZN(n8) );
  MUX21X1 U60 ( .IN1(n48), .IN2(num[0]), .S(load_data), .Q(n46) );
  MUX21X1 U61 ( .IN1(N24), .IN2(Rn[3]), .S(n14), .Q(n31) );
  NAND2X0 U62 ( .IN1(N27), .IN2(n12), .QN(n15) );
  INVX0 U63 ( .INP(n20), .ZN(n12) );
  MUX21X1 U64 ( .IN1(n72), .IN2(num[6]), .S(load_data), .Q(n20) );
  INVX0 U65 ( .INP(find_next_sq), .ZN(n14) );
  NOR2X0 U99 ( .IN1(n3), .IN2(n4), .QN(n118) );
  INVX0 U100 ( .INP(incr_delta), .ZN(n4) );
  XNOR2X1 U101 ( .IN1(n3), .IN2(incr_delta), .Q(N19) );
endmodule


module sqrt_controller_test_1 ( start, finish, clk, clear, ready, load_data, 
        incr_delta, find_next_sq, test_si, test_so, test_se );
  input start, finish, clk, clear, test_si, test_se;
  output ready, load_data, incr_delta, find_next_sq, test_so;
  wire   state_0_, n5, n1, n2, n3, n4;
  wire   [1:0] next_state;

  SDFFARX1 state_reg_0_ ( .D(next_state[0]), .SI(test_si), .SE(test_se), .CLK(
        clk), .RSTB(n5), .Q(state_0_), .QN(n1) );
  SDFFARX1 state_reg_1_ ( .D(next_state[1]), .SI(state_0_), .SE(test_se), 
        .CLK(clk), .RSTB(n5), .Q(test_so) );
  AO21X1 U1 ( .IN1(test_so), .IN2(n2), .IN3(state_0_), .Q(next_state[1]) );
  NAND2X0 U2 ( .IN1(n3), .IN2(n4), .QN(next_state[0]) );
  INVX0 U3 ( .INP(clear), .ZN(n5) );
  INVX0 U4 ( .INP(n3), .ZN(load_data) );
  NAND2X0 U5 ( .IN1(start), .IN2(ready), .QN(n3) );
  NOR2X0 U6 ( .IN1(state_0_), .IN2(test_so), .QN(ready) );
  AND2X1 U7 ( .IN1(test_so), .IN2(state_0_), .Q(incr_delta) );
  INVX0 U8 ( .INP(n4), .ZN(find_next_sq) );
  NAND3X0 U9 ( .IN1(n2), .IN2(n1), .IN3(test_so), .QN(n4) );
  INVX0 U10 ( .INP(finish), .ZN(n2) );
endmodule


module sqrt_Top ( start, clk, clear, num, result, ready, reset, test_si, 
        test_so, test_se );
  input [6:0] num;
  output [3:0] result;
  input start, clk, clear, reset, test_si, test_se;
  output ready, test_so;
  wire   result_3_, result_2_, n2, load_data, incr_delta, find_next_sq, n4, n6,
         n7, n8;
  assign result[3] = 1'b0;
  assign result[2] = 1'b0;
  assign result[1] = 1'b0;

  sqrt_controller_test_1 m1 ( .start(start), .finish(test_so), .clk(clk), 
        .clear(clear), .ready(ready), .load_data(load_data), .incr_delta(
        incr_delta), .find_next_sq(find_next_sq), .test_si(test_si), .test_so(
        n4), .test_se(n7) );
  sqrt_data_path_test_1 m2 ( .num(num), .load_data(load_data), .incr_delta(
        incr_delta), .find_next_sq(find_next_sq), .clk(clk), .finish(test_so), 
        .result({result_3_, result_2_, n2, result[0]}), .reset(reset), 
        .test_si(n4), .test_se(n7) );
  INVX0 U2 ( .INP(n8), .ZN(n6) );
  INVX0 U3 ( .INP(n6), .ZN(n7) );
  DELLN2X2 U4 ( .INP(test_se), .Z(n8) );
endmodule

