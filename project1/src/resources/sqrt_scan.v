/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Expert(TM) in wire load mode
// Version   : O-2018.06-SP1
// Date      : Thu Feb 21 21:58:41 2019
/////////////////////////////////////////////////////////////


module sqrt_data_path_DW01_add_0 ( A, B, CI, SUM, CO );
  input [6:0] A;
  input [6:0] B;
  output [6:0] SUM;
  input CI;
  output CO;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11;

  XOR2X1 U1 ( .IN1(A[6]), .IN2(n1), .Q(SUM[6]) );
  NOR2X0 U2 ( .IN1(n2), .IN2(n3), .QN(n1) );
  INVX0 U3 ( .INP(A[5]), .ZN(n3) );
  XNOR2X1 U4 ( .IN1(A[5]), .IN2(n2), .Q(SUM[5]) );
  OAI22X1 U5 ( .IN1(n4), .IN2(A[4]), .IN3(n5), .IN4(B[4]), .QN(n2) );
  AND2X1 U6 ( .IN1(A[4]), .IN2(n4), .Q(n5) );
  XOR3X1 U7 ( .IN1(B[4]), .IN2(A[4]), .IN3(n4), .Q(SUM[4]) );
  AO22X1 U8 ( .IN1(A[3]), .IN2(n6), .IN3(B[3]), .IN4(n7), .Q(n4) );
  OR2X1 U9 ( .IN1(n6), .IN2(A[3]), .Q(n7) );
  XOR3X1 U10 ( .IN1(B[3]), .IN2(A[3]), .IN3(n6), .Q(SUM[3]) );
  AO22X1 U11 ( .IN1(A[2]), .IN2(n8), .IN3(B[2]), .IN4(n9), .Q(n6) );
  OR2X1 U12 ( .IN1(n8), .IN2(A[2]), .Q(n9) );
  XOR3X1 U13 ( .IN1(B[2]), .IN2(A[2]), .IN3(n8), .Q(SUM[2]) );
  AO22X1 U14 ( .IN1(A[1]), .IN2(n10), .IN3(B[1]), .IN4(n11), .Q(n8) );
  OR2X1 U15 ( .IN1(A[1]), .IN2(n10), .Q(n11) );
  XOR3X1 U16 ( .IN1(B[1]), .IN2(A[1]), .IN3(n10), .Q(SUM[1]) );
  AND2X1 U17 ( .IN1(B[0]), .IN2(A[0]), .Q(n10) );
  XOR2X1 U18 ( .IN1(B[0]), .IN2(A[0]), .Q(SUM[0]) );
endmodule


module sqrt_data_path_test_1 ( num, load_data, incr_delta, find_next_sq, clk, 
        finish, result, reset, test_si, test_se );
  input [6:0] num;
  output [3:0] result;
  input load_data, incr_delta, find_next_sq, clk, reset, test_si, test_se;
  output finish;
  wire   n73, n72, n71, n70, n69, n68, n67, n66, n65, n64, n63, n62, n61, n60,
         n59, n58, n57, n56, n55, n54, N27, N26, N25, N24, N23, N22, N21, N19,
         N18, N17, N16, n4, n78, n81, n87, n1, n2, n3, n5, n6, n7, n8, n9, n10,
         n11, n12, n13, n14, n15, n16, n17, n18, n19, n20, n21, n22, n23, n24,
         n25, n26, n27, n28, n29, n30, n31, n32, n33, n34, n35, n36, n37, n38,
         n39, n40, n41, n42, n43, n44, n45, n46, n47, n48, n49, n50, n51, n52,
         n53, n74, n75, n76;
  wire   [6:0] Rn;

  sqrt_data_path_DW01_add_0 add_20_S2 ( .A(Rn), .B({1'b0, 1'b0, N16, N17, N18, 
        N19, n4}), .CI(1'b0), .SUM({N27, N26, N25, N24, N23, N22, N21}) );
  SDFFX1 Rdata_reg_6_ ( .D(n73), .SI(n75), .SE(test_se), .CLK(clk), .Q(n76) );
  SDFFX1 Rdata_reg_5_ ( .D(n72), .SI(n74), .SE(test_se), .CLK(clk), .Q(n75) );
  SDFFX1 Rdata_reg_4_ ( .D(n71), .SI(n53), .SE(test_se), .CLK(clk), .Q(n74) );
  SDFFX1 Rdata_reg_3_ ( .D(n70), .SI(n50), .SE(test_se), .CLK(clk), .Q(n53) );
  SDFFX1 Rdata_reg_2_ ( .D(n69), .SI(n51), .SE(test_se), .CLK(clk), .Q(n50) );
  SDFFX1 Rdata_reg_1_ ( .D(n68), .SI(n52), .SE(test_se), .CLK(clk), .Q(n51) );
  SDFFX1 Rdata_reg_0_ ( .D(n67), .SI(test_si), .SE(test_se), .CLK(clk), .Q(n52) );
  SDFFX1 Rdelta_reg_0_ ( .D(n66), .SI(n76), .SE(test_se), .CLK(clk), .Q(n4) );
  SDFFX1 Rdelta_reg_1_ ( .D(n65), .SI(n4), .SE(test_se), .CLK(clk), .Q(
        result[0]) );
  SDFFX1 Rdelta_reg_2_ ( .D(n64), .SI(result[0]), .SE(test_se), .CLK(clk), .Q(
        result[1]) );
  SDFFX1 Rdelta_reg_3_ ( .D(n63), .SI(result[1]), .SE(test_se), .CLK(clk), .Q(
        result[2]), .QN(n1) );
  SDFFX1 Rdelta_reg_4_ ( .D(n62), .SI(result[2]), .SE(test_se), .CLK(clk), .Q(
        result[3]) );
  SDFFX1 Rn_reg_0_ ( .D(n61), .SI(result[3]), .SE(test_se), .CLK(clk), .Q(
        Rn[0]), .QN(n87) );
  SDFFX1 Rn_reg_1_ ( .D(n60), .SI(Rn[0]), .SE(test_se), .CLK(clk), .Q(Rn[1])
         );
  SDFFX1 Rn_reg_2_ ( .D(n59), .SI(Rn[1]), .SE(test_se), .CLK(clk), .Q(Rn[2]), 
        .QN(n81) );
  SDFFX1 Rn_reg_3_ ( .D(n58), .SI(Rn[2]), .SE(test_se), .CLK(clk), .Q(Rn[3])
         );
  SDFFX1 Rn_reg_4_ ( .D(n57), .SI(Rn[3]), .SE(test_se), .CLK(clk), .Q(Rn[4])
         );
  SDFFX1 Rn_reg_5_ ( .D(n56), .SI(Rn[4]), .SE(test_se), .CLK(clk), .Q(Rn[5])
         );
  SDFFX1 Rn_reg_6_ ( .D(n55), .SI(Rn[5]), .SE(test_se), .CLK(clk), .Q(Rn[6])
         );
  SDFFX1 finish_reg ( .D(n54), .SI(Rn[6]), .SE(test_se), .CLK(clk), .Q(finish), 
        .QN(n78) );
  MUX21X1 U2 ( .IN1(n76), .IN2(num[6]), .S(n2), .Q(n73) );
  MUX21X1 U3 ( .IN1(n75), .IN2(num[5]), .S(n2), .Q(n72) );
  MUX21X1 U4 ( .IN1(n74), .IN2(num[4]), .S(n2), .Q(n71) );
  MUX21X1 U5 ( .IN1(n53), .IN2(num[3]), .S(n2), .Q(n70) );
  MUX21X1 U6 ( .IN1(n50), .IN2(num[2]), .S(n2), .Q(n69) );
  MUX21X1 U7 ( .IN1(n51), .IN2(num[1]), .S(n2), .Q(n68) );
  MUX21X1 U8 ( .IN1(n52), .IN2(num[0]), .S(n2), .Q(n67) );
  AND2X1 U9 ( .IN1(load_data), .IN2(n3), .Q(n2) );
  OR2X1 U10 ( .IN1(reset), .IN2(n4), .Q(n66) );
  MUX21X1 U11 ( .IN1(n5), .IN2(result[0]), .S(n6), .Q(n65) );
  NAND2X0 U12 ( .IN1(result[0]), .IN2(n3), .QN(n5) );
  AO22X1 U13 ( .IN1(result[1]), .IN2(n6), .IN3(n7), .IN4(n8), .Q(n64) );
  AO22X1 U14 ( .IN1(result[2]), .IN2(n6), .IN3(n7), .IN4(n9), .Q(n63) );
  AO22X1 U15 ( .IN1(result[3]), .IN2(n6), .IN3(n7), .IN4(n10), .Q(n62) );
  NOR2X0 U16 ( .IN1(n6), .IN2(reset), .QN(n7) );
  NOR2X0 U17 ( .IN1(incr_delta), .IN2(reset), .QN(n6) );
  MUX21X1 U18 ( .IN1(n11), .IN2(Rn[0]), .S(n12), .Q(n61) );
  NAND2X0 U19 ( .IN1(n3), .IN2(n13), .QN(n11) );
  INVX0 U20 ( .INP(reset), .ZN(n3) );
  AO22X1 U21 ( .IN1(n12), .IN2(Rn[1]), .IN3(N22), .IN4(n14), .Q(n60) );
  AO22X1 U22 ( .IN1(n12), .IN2(Rn[2]), .IN3(N23), .IN4(n14), .Q(n59) );
  AO22X1 U23 ( .IN1(n12), .IN2(Rn[3]), .IN3(N24), .IN4(n14), .Q(n58) );
  AO22X1 U24 ( .IN1(n12), .IN2(Rn[4]), .IN3(N25), .IN4(n14), .Q(n57) );
  AO22X1 U25 ( .IN1(n12), .IN2(Rn[5]), .IN3(N26), .IN4(n14), .Q(n56) );
  AO22X1 U26 ( .IN1(n12), .IN2(Rn[6]), .IN3(N27), .IN4(n14), .Q(n55) );
  NOR2X0 U27 ( .IN1(n12), .IN2(reset), .QN(n14) );
  NAND2X0 U28 ( .IN1(n15), .IN2(n16), .QN(n54) );
  NAND3X0 U29 ( .IN1(n12), .IN2(Rn[6]), .IN3(n17), .QN(n16) );
  NOR2X0 U30 ( .IN1(find_next_sq), .IN2(reset), .QN(n12) );
  MUX21X1 U31 ( .IN1(n18), .IN2(n78), .S(reset), .Q(n15) );
  NOR2X0 U32 ( .IN1(n19), .IN2(n20), .QN(n18) );
  OA221X1 U33 ( .IN1(n21), .IN2(n22), .IN3(n17), .IN4(n23), .IN5(n24), .Q(n20)
         );
  AO221X1 U34 ( .IN1(n21), .IN2(n22), .IN3(n25), .IN4(n26), .IN5(n27), .Q(n24)
         );
  OA222X1 U35 ( .IN1(n25), .IN2(n26), .IN3(n28), .IN4(n29), .IN5(n30), .IN6(
        n31), .Q(n27) );
  INVX0 U36 ( .INP(n32), .ZN(n31) );
  INVX0 U37 ( .INP(n33), .ZN(n30) );
  MUX21X1 U38 ( .IN1(Rn[3]), .IN2(N24), .S(find_next_sq), .Q(n29) );
  NOR2X0 U39 ( .IN1(n32), .IN2(n33), .QN(n28) );
  AO22X1 U40 ( .IN1(n34), .IN2(n35), .IN3(n36), .IN4(n37), .Q(n33) );
  OR2X1 U41 ( .IN1(n35), .IN2(n34), .Q(n37) );
  MUX21X1 U42 ( .IN1(n81), .IN2(n38), .S(find_next_sq), .Q(n36) );
  INVX0 U43 ( .INP(N23), .ZN(n38) );
  AO22X1 U44 ( .IN1(n39), .IN2(n40), .IN3(n41), .IN4(n42), .Q(n35) );
  MUX21X1 U45 ( .IN1(n87), .IN2(n13), .S(find_next_sq), .Q(n42) );
  INVX0 U46 ( .INP(N21), .ZN(n13) );
  OA21X1 U47 ( .IN1(n39), .IN2(n40), .IN3(n43), .Q(n41) );
  MUX21X1 U48 ( .IN1(n52), .IN2(num[0]), .S(load_data), .Q(n43) );
  INVX0 U49 ( .INP(n44), .ZN(n40) );
  MUX21X1 U50 ( .IN1(Rn[1]), .IN2(N22), .S(find_next_sq), .Q(n44) );
  MUX21X1 U51 ( .IN1(n51), .IN2(num[1]), .S(load_data), .Q(n39) );
  MUX21X1 U52 ( .IN1(n50), .IN2(num[2]), .S(load_data), .Q(n34) );
  MUX21X1 U53 ( .IN1(n53), .IN2(num[3]), .S(load_data), .Q(n32) );
  MUX21X1 U54 ( .IN1(Rn[4]), .IN2(N25), .S(find_next_sq), .Q(n26) );
  INVX0 U55 ( .INP(n45), .ZN(n25) );
  MUX21X1 U56 ( .IN1(n74), .IN2(num[4]), .S(load_data), .Q(n45) );
  MUX21X1 U57 ( .IN1(Rn[6]), .IN2(N27), .S(find_next_sq), .Q(n23) );
  MUX21X1 U58 ( .IN1(Rn[5]), .IN2(N26), .S(find_next_sq), .Q(n22) );
  INVX0 U59 ( .INP(n46), .ZN(n21) );
  MUX21X1 U60 ( .IN1(n75), .IN2(num[5]), .S(load_data), .Q(n46) );
  AND3X1 U61 ( .IN1(find_next_sq), .IN2(N27), .IN3(n17), .Q(n19) );
  INVX0 U62 ( .INP(n47), .ZN(n17) );
  MUX21X1 U63 ( .IN1(n76), .IN2(num[6]), .S(load_data), .Q(n47) );
  XOR2X1 U64 ( .IN1(result[0]), .IN2(incr_delta), .Q(N19) );
  MUX21X1 U65 ( .IN1(result[1]), .IN2(n8), .S(incr_delta), .Q(N18) );
  XOR2X1 U66 ( .IN1(result[0]), .IN2(result[1]), .Q(n8) );
  MUX21X1 U67 ( .IN1(result[2]), .IN2(n9), .S(incr_delta), .Q(N17) );
  XOR2X1 U68 ( .IN1(n48), .IN2(n1), .Q(n9) );
  MUX21X1 U69 ( .IN1(result[3]), .IN2(n10), .S(incr_delta), .Q(N16) );
  XOR2X1 U70 ( .IN1(result[3]), .IN2(n49), .Q(n10) );
  NOR2X0 U71 ( .IN1(n1), .IN2(n48), .QN(n49) );
  NAND2X0 U72 ( .IN1(result[1]), .IN2(result[0]), .QN(n48) );
endmodule


module sqrt_controller_test_1 ( start, finish, clk, clear, ready, load_data, 
        incr_delta, find_next_sq, test_si, test_so, test_se );
  input start, finish, clk, clear, test_si, test_se;
  output ready, load_data, incr_delta, find_next_sq, test_so;
  wire   state_0_, n5, n1, n2, n3, n4;
  wire   [1:0] next_state;

  SDFFARX1 state_reg_0_ ( .D(next_state[0]), .SI(test_si), .SE(test_se), .CLK(
        clk), .RSTB(n5), .Q(state_0_), .QN(n1) );
  SDFFARX1 state_reg_1_ ( .D(next_state[1]), .SI(state_0_), .SE(test_se), 
        .CLK(clk), .RSTB(n5), .Q(test_so) );
  AO21X1 U1 ( .IN1(test_so), .IN2(n2), .IN3(state_0_), .Q(next_state[1]) );
  NAND2X0 U2 ( .IN1(n3), .IN2(n4), .QN(next_state[0]) );
  INVX0 U3 ( .INP(clear), .ZN(n5) );
  INVX0 U4 ( .INP(n3), .ZN(load_data) );
  NAND2X0 U5 ( .IN1(start), .IN2(ready), .QN(n3) );
  NOR2X0 U6 ( .IN1(state_0_), .IN2(test_so), .QN(ready) );
  AND2X1 U7 ( .IN1(test_so), .IN2(state_0_), .Q(incr_delta) );
  INVX0 U8 ( .INP(n4), .ZN(find_next_sq) );
  NAND3X0 U9 ( .IN1(n2), .IN2(n1), .IN3(test_so), .QN(n4) );
  INVX0 U10 ( .INP(finish), .ZN(n2) );
endmodule


module sqrt_Top ( start, clk, clear, num, result, ready, reset, test_si, 
        test_so, test_se );
  input [6:0] num;
  output [3:0] result;
  input start, clk, clear, reset, test_si, test_se;
  output ready, test_so;
  wire   load_data, incr_delta, find_next_sq, n2, n4, n5, n6;

  sqrt_controller_test_1 m1 ( .start(start), .finish(test_so), .clk(clk), 
        .clear(clear), .ready(ready), .load_data(load_data), .incr_delta(
        incr_delta), .find_next_sq(find_next_sq), .test_si(test_si), .test_so(
        n2), .test_se(n5) );
  sqrt_data_path_test_1 m2 ( .num(num), .load_data(load_data), .incr_delta(
        incr_delta), .find_next_sq(find_next_sq), .clk(clk), .finish(test_so), 
        .result(result), .reset(reset), .test_si(n2), .test_se(n5) );
  INVX0 U1 ( .INP(n6), .ZN(n4) );
  INVX0 U2 ( .INP(n4), .ZN(n5) );
  DELLN2X2 U3 ( .INP(test_se), .Z(n6) );
endmodule

